<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', "Delicia" );


/** Имя пользователя MySQL */
define( 'DB_USER', "root" );


/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', "" );


/** Имя сервера MySQL */
define( 'DB_HOST', "localhost" );


/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );


/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@CWD HbUV]fuICpjD%fL4?<ED9{PA3Wtxb}<3L`U Vw+,*7$g`<q`*pO)Mx0KAz-' );

define( 'SECURE_AUTH_KEY',  ';a=DStw3V=o5B0#?,Ad^khUnOuR9bKfT7iKz[Zkvij{an)|Zje0$j?7^]^~DK.+/' );

define( 'LOGGED_IN_KEY',    'Z2XErjpE1B{,^X+52kee%`UAVs+0s~(;djFknx=aIo{w[64=+u9brgeDf&-,uqP9' );

define( 'NONCE_KEY',        '7ox<wBQa#&W>;1vD&2 gAb%lV_[R=))uV KW;Dk_rexK<jF(x8eUW~82)-=al)p<' );

define( 'AUTH_SALT',        '~sCVOx!_aS{&Uh{~;Pp8m~#2S532_$kJR@YGG8/{{j3$PJ<3Ub5f=4PV$qIO8U@z' );

define( 'SECURE_AUTH_SALT', 'kV)T/2|rINi| I*:AS|2}fjzb6*w,mw0y)5n^?l_wz5rOcPd<8Qg!!]aE{6w1I8*' );

define( 'LOGGED_IN_SALT',   'Q|vXro{oj$D}0d-eZkUp&IwYxkf!|Sr;aL`P-b8$I*66m9_Y/xd;rADqdB@F:?/c' );

define( 'NONCE_SALT',       '~1Zwp5_Uci;V> E/^-mR+H/QnCy`&p^;ifV#r6+kC:L>mrkmxkIw<h8=)a>7X5<j' );


/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';


/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
