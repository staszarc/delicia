<?php
/*
Template Name: Articles
Template Post Type: post, page
 */ ?>
<?php setPostViews(get_the_ID()); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>
<body class="articles" style="color:#111;">
<section class="container top">

    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</section>
<section class="articles">
    <?php

    while( have_posts() ) : the_post();
        $more = 1; // отображаем полностью всё содержимое поста
        the_content();
    endwhile;?>
    <div class="container">
        <h4 class="autoplay_h">
            Читать дальше:
        </h4>
        <div class="autoplay">
            <?php
            $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
            $args = array(

                'posts_per_page' => 6, // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
                'paged'          => $current_page, // текущая страница
                'cat' => -22
            );
            query_posts( $args );

            $wp_query->is_archive = true;
            $wp_query->is_home = false;

            while(have_posts()): the_post();
                ?>
                <a href="<?php echo get_permalink(); ?>" class="articles-slider">
                    <?php if ( !is_single() ) { ?>
                        <div class="post-thumbnail" style="background: url(<?php echo get_the_post_thumbnail_url() ?>); ">
                            <?php the_post_thumbnail(); ?>
                        </div><!-- .post-thumbnail -->
                    <?php }?>

                    <p class="autoplay__title"> <?php the_title() ?></p>
                </a>
            <?php endwhile;?>
        </div>
    </div>

</section>
<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function(){
        jQuery('.autoplay').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2
                    }
                },


                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 1
                    }
                }
            ]
        });
    });
</script>
</body>
<footer>

    <?php require 'footer.php'?>
</footer>
</html>
