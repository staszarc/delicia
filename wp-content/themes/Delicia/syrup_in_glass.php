<?php /*Template Name: Syrup In Glasses Bottle */?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>
<body class="shop__syrups_in_glass_bottle">
<section class="container top">

    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</section>
<section class="container">
    <h1>Продукция</h1>
    <?php
    echo do_shortcode('[product_category category="syrups_in_glass_bottle" columns="3" paginate=true per_page="12"]');
    ?>

</section>
</body>
<footer>
    <?php wp_footer(); ?>
    <?php require 'footer.php'?>
</footer>
</html>

