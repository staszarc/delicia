<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<div class="row d-flex justify-content-between align-items-center shop_settings">
    <div class="shop-nav row d-flex align-items-center" style="float:left; color: black">
        <a href="/shop" class="shop__nav">
            Все товары
        </a>
        <a href="/shop/toppings" class="toppings__nav">
            Топпинги
        </a>
        <a href="/shop/syrups" class="syrups__nav">
            Сиропы в ПЭТ
        </a>
        <a href="/shop/syrups_in_glass_bottle/" class="syrups-in-glasses-bottle__nav">
            Сиропы в стекле
        </a>
        <a href="/shop/jams" class="jams__nav">
            Джемы
        </a>
    </div>
    <div class="shop-nav__selector row align-items-center">
        <p>Категория: </p>
        <select class="" name="formal" onchange="javascript:handleSelect(this)">

            <option value="#">Выбор категории</option>
            <option value="/shop">Все товары</option>
            <option value="/shop/toppings">Топпинги</option>
            <option value="/shop/syrups">Сиропы в ПЭТ</option>
            <option value="/shop/syrups-in-glass-bottle">Сиропы в стекле</option>
            <option value="/shop/jams">Джемы</option>
        </select>
    </div>

    <form class="woocommerce-ordering" method="get">
        <div class="row align-items-center">
            <p>Сортировать: </p>
            <select name="orderby" class="orderby" aria-label="<?php esc_attr_e( 'Shop order', 'woocommerce' ); ?>">
                <?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
                    <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="paged" value="1" />
            <?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>

        </div>
    </form>

</div>
<script type="text/javascript">
    function handleSelect(elm)
    {
        window.location = elm.value;
    }
</script>
