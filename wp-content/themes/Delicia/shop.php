<?php /*Template Name: Shop */?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>
<body class="shop">
    <section class="container top">

        <?php woocommerce_breadcrumb (); ?>
    </section>
    <section class="container">
        <h1>Продукция</h1>
        <?php
        echo do_shortcode('[products paginate=true per_page=12 columns="3"]');
        ?>

    </section>
</body>
<footer>
    <?php wp_footer(); ?>
    <?php require 'footer.php'?>
</footer>
</html>

