<?php
/*
 * Template name: Акции
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>

<body class="stocks" style=" background-color: #f4f4f4;">
<section class="container top">

    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</section>

<section class="container">
    <h1>Акции</h1>
    <?php
    $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
    $args = array(

        'posts_per_page' => 5, // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
        'paged'          => $current_page, // текущая страница
        'category_name' => 'stocks',
        'orderby' => 'modified'
    );
    query_posts( $args );

    $wp_query->is_archive = true;
    $wp_query->is_home = false;

    while(have_posts()): the_post();
        ?>

        <div class="blog-miniature">


            <div class="blog-miniature__bottom">
                <h2 class="blog__title"><?php the_title() ?></h2>
                <p class="blog__pre-text"><?php the_excerpt() ?></p>


            </div>
            <?php if ( !is_single() ) { ?>
                <div class="post-thumbnail" style="background: url(<?php echo get_the_post_thumbnail_url() ?>); ">
                    <?php the_post_thumbnail(); ?>
                </div><!-- .post-thumbnail -->
            <?php }?>
        </div>
    <?php endwhile;?>
    <?php
    if( function_exists('wp_pagenavi') ) wp_pagenavi(); // функция постраничной навигации
    if (function_exists('wp_corenavi')) wp_corenavi(); ?>
</section>

</body>
<footer>
    <?php wp_footer(); ?>
    <?php require 'footer.php'?>
</footer>
</html>
