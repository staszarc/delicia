<?php /*Template Name: Product cart
Template Post Type: product*/?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>
<body class="" style="color:black">
<section class="container top">
    <?php woocommerce_breadcrumb ($args); ?>
</section>
<section class="container product-cart">
    <?php
    global $woocommerce;
    $product = new WC_Product(get_the_ID());
    ?>
    <div class="product_cart__main row ">
        <div class="product-cart__img col-sm-6 col-12">
            <div class="first-elipse" style="
            <?php if (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/toppings/\" rel=\"tag\">Топпинги</a>") {
                echo "background-color: #aa96e3!important; ";
            }
            elseif (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/syrups/\" rel=\"tag\">Сиропы</a>"){
                echo "background-color: #c2f0b3!important; ";
            }
            elseif (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/syrups_in_glass_bottle/\" rel=\"tag\">Сиропы в стекле</a>"){
                echo "background-color: #ade7e7!important;";
            }
            elseif (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/jams/\" rel=\"tag\">Джемы</a>"){
                echo "background-color: #2286ea!important;";
            }
            ?> ">

            </div>
            <div class="second-elipse">

            </div>
            <?php echo woocommerce_get_product_thumbnail();?>

        </div>
        <div class="product-cart__info col-sm-6 col-12">
            <div class="product-cart__title"> <?php woocommerce_template_single_title();?></div>
            <div class="product-cart__descr">
                <?php woocommerce_template_single_excerpt();?>

            </div>
            <div class=" d-flex justify-content-sm-between product_cart__main__price-and-cart align-items-center">
                <?php woocommerce_template_single_price();?>
                <div style="<?php if (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/toppings/\" rel=\"tag\">Топпинги</a>") {
                    echo "background-color: #aa96e3!important; ";
                }
                elseif (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/syrups/\" rel=\"tag\">Сиропы</a>"){
                    echo "background-color: #c2f0b3!important; ";
                }
                elseif (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/syrups_in_glass_bottle/\" rel=\"tag\">Сиропы в стекле</a>"){
                    echo "background-color: #ade7e7!important;";
                }
                elseif (wc_get_product_category_list($product->get_id()) == "<a href=\"http://delicia.cube3.world/shop/jams/\" rel=\"tag\">Джемы</a>"){
                    echo "background-color: #2286ea!important;";
                }
                ?> ">
                    <?php woocommerce_template_single_add_to_cart();?>
                </div>

            </div>
        </div>

    </div>
    <h1>
        похожие топпинги
    </h1>
    <div class="product-cart__slider">
        <?php echo do_shortcode("[wcpcsu id='159']"); ?>

    </div>
    <h1 class="h1">Рецепты</h1>
    <div class="product-cart__posts">
        <?php
        $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
        $args = array(

            'posts_per_page' => 3, // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
            'paged'          => $current_page, // текущая страница
             'cat' => -22
        );
        query_posts( $args );

        $wp_query->is_archive = true;
        $wp_query->is_home = false;

        while(have_posts()): the_post();
            ?>

            <div class="blog-miniature">
                <?php if ( !is_single() ) { ?>
                    <div class="post-thumbnail" style="background: url(<?php echo get_the_post_thumbnail_url() ?>); ">
                        <?php the_post_thumbnail(); ?>
                    </div><!-- .post-thumbnail -->
                <?php }?>

                <div class="blog-miniature__bottom">
                    <div class="blog-info justify-content-between d-flex" >
                        <p class="blog-author"><?php the_author();?></p>
                        <p class="blog-date"><?php the_time('j F Y')?></p>
                    </div>
                    <h2 class="blog__title"><?php the_title() ?></h2>
                    <p class="blog__pre-text"><?php the_excerpt() ?></p>
                    <div class="blog-miniature__info justify-content-between d-flex">
                        <a href="<?php echo get_permalink(); ?>" class="blog__link">  Читать → </a>
                    </div>

                </div>
            </div>
        <?php endwhile;?>

    </div>
</section>
</body>
<footer>
    <?php wp_footer(); ?>
    <?php require 'footer.php'?>
<script>

</script>
</footer>
</html>



