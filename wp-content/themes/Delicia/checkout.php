<?php
/*
 * Template name: Checkout
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>

<body class="checkout" style="color:black;">
<section class="container top">

    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</section>


<?php echo do_shortcode('[woocommerce_checkout]');?>

</body>
<footer>
    <?php wp_footer(); ?>
    <?php require 'footer.php'?>
</footer>
</html>
