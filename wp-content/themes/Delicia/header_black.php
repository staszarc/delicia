<header class="black_menu">
    <div class="topnav" id="myTopnav">
        <div class="container">
            <div class="responsive-menu">
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <i class="fa fa-bars black_icon_color"></i>
                </a>
                <a class="logo" href="/"><img src="/wp-content/themes/Delicia/assets/img/logo_black.png" alt="logo"></a>

                <a href="#" class="shopping-bag">
                    <i class="fas fa-shopping-bag black_icon_color"></i>
                </a>
                <div class="black lang"><? echo do_shortcode('[gtranslate]'); ?></div>
            </div>
            <div class="main-menu  align-items-center ">
                <div class="col-1-sm justify-content-start main_menu__lang">
                    <div class="black"><? echo do_shortcode('[gtranslate]'); ?></div>
                </div>
                <div class="col-3-sm main-menu__center justify-content-around align-items-center">
                    <a href="/about" class="black">о нас</a>
                    <a href="/shop" class="black">продукция</a>
                    <a href="/stocks" class="black">акции</a>
                </div>
                <a class="col-3-sm main_menu__logo" href="/"><img src="/wp-content/themes/Delicia/assets/img/logo_black.png" alt="logo"></a>
                <div class="col-4-sm main-menu__center justify-content-around align-items-center">
                    <a href="/blog" class="black">блог</a>
                    <a href="/cooperation" class="black">сотрудничество</a>
                    <a href="/#contacts" class="black">контакты</a>
                </div>
                <div class="col-1-sm d-flex justify-content-end main_menu__bag">
                    <a href="/cart"><i class="fas fa-shopping-bag black_icon_color" style="color:white;"></i></a>
                </div>
            </div>
        </div>
    </div>
</header>
