<?php /* Template Name: About */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="error-404">

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>
<body class="">
    <div class="container">
        <div class="row">
            <div class="text-404 offset-md-1 col-md-6 d-flex flex-column justify-content-center">
                <h1>
                    Ошибка 404. страница не найдена
                </h1>
                <p>
                    Возможно она была перемещена  или вы просто неверно указали адрес страницы
                </p>
                <a class="btn" href="/">
                    НА ГЛАВНУЮ
                </a>
            </div>
            <div class="col-md-5 error-img d-flex justify-content-center align-items-center">
                <img src="/wp-content/themes/Delicia/assets/img/best-1.png" alt="best-1">
            </div>
        </div>
    </div>


</body>

<footer>
    <?php wp_footer(); ?>
</footer>
</html>
