<?php
/**
 * The base (and only) template
 *
 * @package WordPress
 * @subpackage Delicia
 */

?><!DOCTYPE html>
<html class="main_html">

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>

<?php require 'header_black.php'?>


<main style="    height: 100%;
    width: 100%;">
<div class="main" id="fullpage">


    <!-- FIRST SECTION WITH PARALLAX EFFECT !-->

    <section class="first-slide section">

        <div class="parallax-bg" id="parallax">

            <div id="container">
                <div class="wrapper-parallax-bg-1">
                    <div class="wrapper-scene-1 z-index-min">
                        <div data-relative-input="true" id="scene" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.2" class="layer">
                                <img src="/wp-content/themes/Delicia/assets/img/first-slide_1.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-scene-2 z-index-min">
                        <div data-relative-input="true" id="scene1" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.3" class="layer">
                                <img src="/wp-content/themes/Delicia/assets/img/first-slide_2.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-scene-3 z-index-min">
                        <div data-relative-input="true" id="scene2" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.7" class="layer">
                                <img src="/wp-content/themes/Delicia/assets/img/first-slide_3.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-scene-4 z-index-min">
                        <div data-relative-input="true" id="scene3" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.5" class="layer">
                                <img src="/wp-content/themes/Delicia/assets/img/first-slide_4.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="wrapper-parallax-bg-2">
                    <div class="wrapper-scene-1 z-index-max">
                        <div data-relative-input="true" id="scene4" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.2" class="layer">
                                <a class="scena-link" href="#"><img src="/wp-content/themes/Delicia/assets/img/first-slide_1.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-scene-2 z-index-max">
                        <div data-relative-input="true" id="scene5" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.3" class="layer">
                                <a class="scena-link" href="#"><img src="/wp-content/themes/Delicia/assets/img/first-slide_2.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-scene-3 z-index-max">
                        <div data-relative-input="true" id="scene6" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.7" class="layer">
                                <a class="scena-link" href="#"><img src="/wp-content/themes/Delicia/assets/img/first-slide_3.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper-scene-4 z-index-max">
                        <div data-relative-input="true" id="scene7" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                            <div data-depth="0.5" class="layer">
                                <a class="scena-link" href="#"><img src="/wp-content/themes/Delicia/assets/img/first-slide_4.png" alt=""></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="   text-center flex-column d-flex justify-content-center first-slide__text">
            <h1>ОРИГИНАЛЬНЫЕ</h1>
            <h2>СИРОПЫ. ТОПИНГИ. НАПОЛНИТЕЛИ.</h2>
        </div>
    </section>


    <!-- Second slide with 2 images and text.

                    WE ARE BEST
     !-->



    <section class="best section">
        <div class="best__main">
            <div class="best__bg-img">
                <img src="/wp-content/themes/Delicia/assets/img/best-1.png" alt="best-1">
                <img src="/wp-content/themes/Delicia/assets/img/best-2.png" alt="best-2">
            </div>
            <div class=" best__text text-center ">
                <h3 class="text-uppercase">
                    Мы производим лУЧШИЕ
                </h3>
                <h4 class="text-uppercase">
                    Вкусо-ароматические наполнители
                </h4>
                <p>
                    ТМ "DELICIA" - производство вкусо-ароматических наполнителей для кондитерской,
                    молочной и хлебобулочной промышленности. Наша компания работает в сфере производства
                    топпингов, джемов и сиропов уже 11 лет. За эти годы на рынке пищевой промышленности,
                    благодаря плодотворной работе и накопленному опыту наших специалистов, мы стали
                    лидерами в производстве натуральных вкусо-ароматических добавок в Украине.
                </p>
            </div>
        </div>
    </section>


    <section class="left sirop section">
        <div class="container">
            <div class="d-flex">
                <div data-relative-input="true" id="best__bg-img" class="col-sm-4 col-lg-5 offset-lg-1 justify-content-center img-container" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">


                    <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/2.png" alt="sirop">

                    <img src="/wp-content/themes/Delicia/assets/img/1.png" alt="sirop">
                </div>
                <div class="product-attribute col-sm-8 col-lg-5 offset-lg-1 offset-min-3 flex-column">
                    <h2>
                        Сироп
                    </h2>
                    <h4>
                    </h4>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>1000 г</em>
                        </div>
                        <em class="col-7 col-md-9">ПЛАСТИК</em>
                    </div>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>100 %</em>
                        </div>
                        <em class="col-7 col-md-9">ОРИГИНАЛЬНЫЙ ПРОДУКТ</em>
                    </div>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>12 МЕС</em>
                        </div>
                        <em class="col-7 col-md-9">ПРИ ТЕМПЕРАТУРЕ
                            ОТ 0°C ДО +25°C</em>
                    </div>



                    <ul class="nav nav-pills  row justify-content-start collapse-drop" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"  id="pills-home-tab modal-1" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">                        <p class="">
                                    Характеристики
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-home-tab modal-2" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">                        <p class="">
                                    
                                </p>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div>
                                <p>
                                    Пищевая ценность на 100г:
                                </p>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Белков</em>
                                    <em class="col-7 col-md-9">0,06 г</em>
                                </div>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Жиров</em>
                                    <em class="col-7 col-md-9">0,03 г</em>
                                </div>
                                <div class="row">
                                    <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                    <em class="col-7 col-md-9">70,5 г</em>
                                </div>
                                <p class="m-20">
                                    Калорийность:
                                </p>
                                <div class="row last-child">
                                    <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                    <em class="col-7 col-md-9">280 ккал</em>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="char" >
                                <p>Наполнитель вкусо-ароматический на основе сахарного сиропа, пастеризованный. </p> <br>
                            </div>
                        </div>
                    </div>




                    <a href="/shop/syrups" class="btn">
                        ДРУГИЕ ВКУСЫ
                    </a>
                </div>


                <!-- Modal -->
                <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong1" style="color: black"  aria-labelledby="exampleModalLongTitle1" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Характеристики</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span style="color: black"  aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <p>
                                        Пищевая ценность на 100г:
                                    </p>
                                    <div class="row align-items-center">
                                        <em class="col-5 attribute-name col-md-3">Белков</em>
                                        <em class="col-7 col-md-9">?? г</em>
                                    </div>
                                    <div class="row align-items-center">
                                        <em class="col-5 attribute-name col-md-3">Жиров</em>
                                        <em class="col-7 col-md-9">?? г</em>
                                    </div>
                                    <div class="row">
                                        <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                        <em class="col-7 col-md-9">64 г</em>
                                    </div>
                                    <p class="m-20">
                                        Калорийность:
                                    </p>
                                    <div class="row last-child">
                                        <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                        <em class="col-7 col-md-9">255 ккал</em>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong2" style="color: black"  aria-labelledby="exampleModalLongTitle2" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Описание</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span style="color: black"  aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="char">
                                    <p>Сиропы отлично подходят в качестве завершающего штриха любого кофейного, алкогольного или безалкогольного напитка, а также сладкого кондитерского изделия. </p> <br>
                                    <p>Благодаря использованию наших натуральных сиропов любые десерты или напитки заиграют новыми ароматами и вкусовыми оттенками.</p>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!---      SECOND SLIDE TOPPING      !-->


    <section class="left right topping section">
        <div class="container">
            <div class="d-flex">
                <div class="product-attribute col-sm-8 col-lg-5 offset-xl-1 offset-min-3-r flex-column">
                    <h2>
                        Топпинг
                    </h2>
                    <h4>
                    </h4>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>600 г</em>
                        </div>
                        <em class="col-7 col-md-9">ПЛАСТИК</em>
                    </div>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>100 %</em>
                        </div>
                        <em class="col-7 col-md-9">ОРИГИНАЛЬНЫЙ ПРОДУКТ</em>
                    </div>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>12 МЕС</em>
                        </div>
                        <em class="col-7 col-md-9">ПРИ ТЕМПЕРАТУРЕ
                            ОТ 0°C ДО +25°C</em>
                    </div>

                    <ul class="nav nav-pills  row justify-content-start collapse-drop" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"  id="pills-home-tab modal-3" data-toggle="pill" href="#pills-home-1" role="tab" aria-controls="pills-home" aria-selected="true">                        <p class="">
                                    Характеристики
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-home-tab modal-4" data-toggle="pill" href="#pills-profile-1" role="tab" aria-controls="pills-profile" aria-selected="false">                        <p class="">
                                    Описание
                                </p>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home-1" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div>
                                <p>
                                    Пищевая ценность на 100г:
                                </p>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Белков</em>
                                    <em class="col-7 col-md-9">1,85 г</em>
                                </div>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Жиров</em>
                                    <em class="col-7 col-md-9">6,75 г</em>
                                </div>
                                <div class="row">
                                    <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                    <em class="col-7 col-md-9">58,25 г</em>
                                </div>
                                <p class="m-20">
                                    Калорийность:
                                </p>
                                <div class="row last-child">
                                    <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                    <em class="col-7 col-md-9">305 ккал</em>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile-1" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="char" >
                                <p>Наполнитель вкусо-ароматический на основе сахарного сиропа, пастеризованный. </p> <br>
                            </div>
                        </div>


                    </div>

                    <a href="/shop/toppings" class="btn">
                        ДРУГИЕ ВКУСЫ
                    </a>

                </div>
                <div data-relative-input="true" id="best__bg-img-2" class=" col-sm-4 col-lg-4 justify-content-center img-container img-container-r" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">


                    <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/4.png" alt="sirop">

                        <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/3.png" alt="sirop">

              </div>


            </div>


            <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong3" style="color: black"  aria-labelledby="exampleModalLongTitle3" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Характеристики</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span style="color: black"  aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <p>
                                    Пищевая ценность на 100г:
                                </p>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Белков</em>
                                    <em class="col-7 col-md-9">?? г</em>
                                </div>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Жиров</em>
                                    <em class="col-7 col-md-9">?? г</em>
                                </div>
                                <div class="row">
                                    <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                    <em class="col-7 col-md-9">64 г</em>
                                </div>
                                <p class="m-20">
                                    Калорийность:
                                </p>
                                <div class="row last-child">
                                    <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                    <em class="col-7 col-md-9">255 ккал</em>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong4" style="color: black"  aria-labelledby="exampleModalLongTitle4" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Описание</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span style="color: black"  aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="char" >
                                <p>Топпинги представляют собой специальные густые добавки для кондитерской промышленности и украшения таких десертов, как торты, пирожные, блины, мороженное и других кондитерских изделий. </p> <br>
                                <p>Они придают блюду не только неповторимый вкус и аромат, а также служат уникальным оформлением при подаче.</p>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </section>





    <section class="left sirop-2 section">
            <div class="container">
                <div class="d-flex">
                    <div data-relative-input="true" id="best__bg-img-3" class="col-sm-4 col-lg-5 offset-lg-1 justify-content-center img-container" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">


                        <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/6.png" alt="sirop">

                        <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/5.png" alt="sirop">
                    </div>
                    <div class="product-attribute col-sm-8 col-lg-5 offset-xl-1 offset-min-3 flex-column">
                        <h2>
                            Сироп
                        </h2>
                        <h4>
                        </h4>
                        <div class="row align-items-center">
                            <div class="product-attribute__first col-5 col-md-3">
                                <em>900 г</em>
                            </div>
                            <em class="col-7 col-md-9">СТЕКЛО</em>
                        </div>
                        <div class="row align-items-center">
                            <div class="product-attribute__first col-5 col-md-3">
                                <em>100 %</em>
                            </div>
                            <em class="col-7 col-md-9">ОРИГИНАЛЬНЫЙ ПРОДУКТ</em>
                        </div>
                        <div class="row align-items-center">
                            <div class="product-attribute__first col-5 col-md-3">
                                <em>12 МЕС</em>
                            </div>
                            <em class="col-7 col-md-9">ПРИ ТЕМПЕРАТУРЕ
                                ОТ 0°C ДО +25°C</em>
                        </div>

                        <ul class="nav nav-pills  row justify-content-start collapse-drop" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active"  id="pills-home-tab modal-5" data-toggle="pill" href="#pills-home-2" role="tab" aria-controls="pills-home" aria-selected="true">                        <p class="">
                                        Характеристики
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab modal-6" data-toggle="pill" href="#pills-profile-2" role="tab" aria-controls="pills-profile" aria-selected="false">                        <p class="">
                                        
                                    </p>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home-2" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div>
                                    <p>
                                        Пищевая ценность на 100г:
                                    </p>
                                    <div class="row align-items-center">
                                        <em class="col-5 attribute-name col-md-3">Белков</em>
                                        <em class="col-7 col-md-9">0,06 г</em>
                                    </div>
                                    <div class="row align-items-center">
                                        <em class="col-5 attribute-name col-md-3">Жиров</em>
                                        <em class="col-7 col-md-9">0,03 г</em>
                                    </div>
                                    <div class="row">
                                        <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                        <em class="col-7 col-md-9">70,5 г</em>
                                    </div>
                                    <p class="m-20">
                                        Калорийность:
                                    </p>
                                    <div class="row last-child">
                                        <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                        <em class="col-7 col-md-9">280 ккал</em>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile-2" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="char" >
                                    <p>Наполнитель вкусо-ароматический на основе сахарного сиропа, пастеризованный. </p> <br>
                                </div>
                            </div>


                        </div>

                        <a href="/shop/syrups_in_glass_bottle" class="btn">
                            ДРУГИЕ ВКУСЫ
                        </a>
                    </div>
                </div>

                <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong5" style="color: black"  aria-labelledby="exampleModalLongTitle5" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Характеристики</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span style="color: black"  aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <p>
                                        Пищевая ценность на 100г:
                                    </p>
                                    <div class="row align-items-center">
                                        <em class="col-5 attribute-name col-md-3">Белков</em>
                                        <em class="col-7 col-md-9">?? г</em>
                                    </div>
                                    <div class="row align-items-center">
                                        <em class="col-5 attribute-name col-md-3">Жиров</em>
                                        <em class="col-7 col-md-9">?? г</em>
                                    </div>
                                    <div class="row">
                                        <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                        <em class="col-7 col-md-9">64 г</em>
                                    </div>
                                    <p class="m-20">
                                        Калорийность:
                                    </p>
                                    <div class="row last-child">
                                        <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                        <em class="col-7 col-md-9">255 ккал</em>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong6" style="color: black"  aria-labelledby="exampleModalLongTitle6" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Описание</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span style="color: black"  aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="char">
                                    <p>Сиропы отлично подходят в качестве завершающего штриха любого кофейного, алкогольного или безалкогольного напитка, а также сладкого кондитерского изделия. </p> <br>
                                    <p>Благодаря использованию наших натуральных сиропов любые десерты или напитки заиграют новыми ароматами и вкусовыми оттенками.</p>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </section>


    <section class="left right jem section">
        <div class="container">
            <div class="d-flex">
                <div class="product-attribute col-sm-8 col-lg-5 offset-xl-2 offset-min-3-r flex-column">
                    <h2>
                        Фруктовый наполнитель
                    </h2>
                    <h4>
                    </h4>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>1000 г</em>
                        </div>
                        <em class="col-7 col-md-9">ПЛАСТИК</em>
                    </div>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>100 %</em>
                        </div>
                        <em class="col-7 col-md-9">ОРИГИНАЛЬНЫЙ ПРОДУКТ</em>
                    </div>
                    <div class="row align-items-center">
                        <div class="product-attribute__first col-5 col-md-3">
                            <em>12 МЕС</em>
                        </div>
                        <em class="col-7 col-md-9">ПРИ ТЕМПЕРАТУРЕ
                            ОТ 0°C ДО +25°C</em>
                    </div>


                    <ul class="nav nav-pills  row justify-content-start collapse-drop" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active"  id="pills-home-tab modal-7" data-toggle="pill" href="#pills-home-3" role="tab" aria-controls="pills-home" aria-selected="true">                        <p class="">
                                    Характеристики
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-home-tab modal-8" data-toggle="pill" href="#pills-profile-3" role="tab" aria-controls="pills-profile" aria-selected="false">                        <p class="">

                                </p>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home-3" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div>
                                <p>
                                    Пищевая ценность на 100г:
                                </p>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Белков</em>
                                    <em class="col-7 col-md-9">0,93 г</em>
                                </div>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Жиров</em>
                                    <em class="col-7 col-md-9">0,07 г</em>
                                </div>
                                <div class="row">
                                    <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                    <em class="col-7 col-md-9">58,03 г</em>
                                </div>
                                <p class="m-20">
                                    Калорийность:
                                </p>
                                <div class="row last-child">
                                    <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                    <em class="col-7 col-md-9">244 ккал</em>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile-3" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="char" >
                                <p>Наполнитель вкусо-ароматический на основе сахарного сиропа, пастеризованный.</p> <br>
                            </div>
                        </div>


                    </div>



                    <a href="/shop/jams" class="btn">
                        ДРУГИЕ ВКУСЫ
                    </a>

                </div>

                <div data-relative-input="true" id="best__bg-img-4" class=" col-sm-4 col-lg-4 justify-content-center img-container img-container-r r-115" style="transform: translate3d(0px, 0px, 0px) rotate(0.0001deg); transform-style: preserve-3d; backface-visibility: hidden; position: relative;">
                    <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/8.png" alt="sirop">
                    <img data-depth="0.2" class="layer" src="/wp-content/themes/Delicia/assets/img/7.png" alt="sirop">
                </div>

            </div>

            <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong7" style="color: black"  aria-labelledby="exampleModalLongTitle7" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Характеристики</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span style="color: black"  aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div>
                                <p>
                                    Пищевая ценность на 100г:
                                </p>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Белков</em>
                                    <em class="col-7 col-md-9">?? г</em>
                                </div>
                                <div class="row align-items-center">
                                    <em class="col-5 attribute-name col-md-3">Жиров</em>
                                    <em class="col-7 col-md-9">?? г</em>
                                </div>
                                <div class="row">
                                    <em class="col-5 attribute-name col-md-3">Углеводов</em>
                                    <em class="col-7 col-md-9">64 г</em>
                                </div>
                                <p class="m-20">
                                    Калорийность:
                                </p>
                                <div class="row last-child">
                                    <em class="col-5 attribute-name col-md-3">На 100 г</em>
                                    <em class="col-7 col-md-9">255 ккал</em>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal" tabindex="-1" role="dialog" id="exampleModalLong8" style="color: black"  aria-labelledby="exampleModalLongTitle8" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Описание</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span style="color: black"  aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="char" >
                                <p>Натуральные джемы отлично подходят
                                    к чаю или кофе как заменитель конфет
                                    и других вредных сладостей.</p> <br>
                                <p>Наши джемы, изготовленные на профессиональном высокотехнологичном оборудовании с максимальным содержанием натурального плодово- ягодного сырья лучшего качества.</p>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="contact section">
            <div class="container offset-md-2 contact__text">
                <h2>
                    КОНТАКТЫ
                </h2>
                <p class="contact_first">
                    Телефон
                </p>
                <a href="tel:+380987777101"> +380 98 777 71 01 </a>

                <p>электронная почта</p>
                <a href="mailto:info.galafrut@gmail.com"> info.galafrut@gmail.com  </a>
                <p class="at_all">
                    Мы делаем вашу жизнь вкуснее!
                </p>

            </div>

                <div class="test-block scroll-gallery" style="    overflow-y: auto;
    overflow-x: hidden;">
                    <div class="list-container" style="    overflow-y: auto;
    overflow-x: hidden;">
                        <ul class="list">
                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                        </ul>
                        <ul class="list"  >
                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                        </ul>
                        <ul class="list"  >
                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                            <li class="li">
                                <img src="/wp-content/themes/Delicia/assets/img/6-layers.png">
                            </li>

                        </ul>


                    </div>



        </div>


    </section>

</div>
</main>








<footer>
    <?php wp_footer(); ?>

</footer>
<script>


    // device detection
    jQuery( document ).ready(function() {
        var ic = 1;
        var windowWidth = window.screen.width < window.outerWidth ?
            window.screen.width : window.outerWidth;
        var mobile = windowWidth < 500;
        while (ic <= 8 && mobile === true){

            var modal = document.getElementById("pills-home-tab modal-" + ic);
            console.log("pills-home-tab modal-" + ic);
            modal.removeAttribute("data-toggle");
            modal.setAttribute("data-toggle", "modal");
            modal.setAttribute("data-target", "#exampleModalLong" + ic);
            ic = ic+1
        }



    });


    var scene = document.getElementById('scene');
    var parallax = new Parallax(scene, {
        selector: '.layer'
    });
    var scene_1 = document.getElementById('scene1');
    var parallax_1 = new Parallax(scene_1, {
        selector: '.layer'
    });
    var scene_2 = document.getElementById('scene2');
    var parallax_2 = new Parallax(scene_2, {
        selector: '.layer'
    });
    var scene_3 = document.getElementById('scene3');
    var parallax_3 = new Parallax(scene_3, {
        selector: '.layer'
    });
    var scene_4 = document.getElementById('scene4');
    var parallax_4 = new Parallax(scene_4, {
        selector: '.layer'
    });
    var scene_5 = document.getElementById('scene5');
    var parallax_5 = new Parallax(scene_5, {
        selector: '.layer'
    });
    var scene_6 = document.getElementById('scene6');
    var parallax_6 = new Parallax(scene_6, {
        selector: '.layer'
    });
    var scene_7 = document.getElementById('scene7');
    var parallax_7 = new Parallax(scene_7, {
        selector: '.layer'
    });
    var scene_8 = document.getElementById('best__bg-img');
    var parallax_8 = new Parallax(scene_8, {
        selector: '.layer'
    });
    var scene_9 = document.getElementById('best__bg-img-2');
    var parallax_9 = new Parallax(scene_9, {
        selector: '.layer'
    });
    var scene_10 = document.getElementById('best__bg-img-3');
    var parallax_10 = new Parallax(scene_10, {
        selector: '.layer'
    });
    var scene_11 = document.getElementById('best__bg-img-4');
    var parallax_11 = new Parallax(scene_11, {
        selector: '.layer'
    });
</script>
<script>
    jQuery(document).ready(function() {
        jQuery('#fullpage').fullpage({
            scrollingSpeed: 1000,
            autoScrolling: true,
            anchors: ['delicia', 'about_us', 'syrup', 'topping','syrups', 'jam', 'contacts'],
            verticalCentered: false,
        });
    });

</script>

<script src="assets/bootstrap/bootstrap.min.js"></script>
</html>
