
<?php
/*
 * Template name: Блог
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php wp_head(); ?>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<?php require 'header_black.php'?>

<body class="blog" style="color:black; background-color: #f4f4f4;">
<section class="container top">

    <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
</section>

<section class="container blog__section">
    <h1>блог</h1>
    <?php
    $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
    $args = array(

        'posts_per_page' => get_option('posts_per_page'), // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
        'paged'          => $current_page, // текущая страница
        'cat' => -22
    );
    query_posts( $args );

    $wp_query->is_archive = true;
    $wp_query->is_home = false;

    while(have_posts()): the_post();
    ?>

    <div class="blog-miniature">
        <a href="<?php echo get_permalink(); ?>" class="blog__link" style="z-index: 1000;"></a>
        <?php if ( !is_single() ) { ?>
            <div  class="post-thumbnail" style="background: url(<?php echo get_the_post_thumbnail_url() ?>); ">
                <?php the_post_thumbnail(); ?>
            </div><!-- .post-thumbnail -->
        <?php }?>

        <div class="blog-miniature__bottom">
            <div class="blog-info justify-content-between d-flex" >
                <p class="blog-author"><?php the_author();?></p>
                <p class="blog-date"><?php the_time('j F Y')?></p>
            </div>
            <h2 class="blog__title"><?php the_title() ?></h2>
            <p class="blog__pre-text"><?php the_excerpt() ?></p>
            <div class="blog-miniature__info justify-content-between d-flex">
                <a href="<?php echo get_permalink(); ?>" class="blog__link">  Читать → </a>
                <p class="blog__view"><?php echo getPostViews(get_the_ID()); ?></p>
            </div>

        </div>
    </div>
           <?php endwhile;?>
    <?php
    if( function_exists('wp_pagenavi') ) wp_pagenavi(); // функция постраничной навигации
    if (function_exists('wp_corenavi')) wp_corenavi(); ?>
</section>

</body>
<footer>
    <?php wp_footer(); ?>
    <?php require 'footer.php'?>
</footer>
</html>
