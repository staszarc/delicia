<header class="header">
    <div class="topnav" id="myTopnav">
        <div class="container">
            <div class="responsive-menu">
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <i class="fa fa-bars"></i>
                </a>
                <a class="logo" href="/"><img src="/wp-content/themes/Delicia/assets/img/logo.png" alt="logo"></a>

                <a href="#" class="shopping-bag">
                    <i class="fas fa-shopping-bag"></i>
                </a>
                <div class=" lang"><? echo do_shortcode('[gtranslate]'); ?></div>
            </div>
            <div class="main-menu align-items-center ">
                <div class="col-1-sm justify-content-start main_menu__lang">
                    <div class=""><? echo do_shortcode('[gtranslate]'); ?></div>
                </div>
                <div class="col-3-sm main-menu__center justify-content-around align-items-center">
                    <a href="/about">о нас</a>
                    <a href="/shop">продукция</a>
                    <a href="/stocks">акции</a>
                </div>
                <a class="col-3-sm main_menu__logo" href="/"><img src="/wp-content/themes/Delicia/assets/img/logo.png" alt="logo"></a>
                <div class="col-4-sm main-menu__center justify-content-around align-items-center">
                    <a href="/blog">блог</a>
                    <a href="/cooperation">сотрудничество</a>
                    <a href="#contacts">контакты</a>
                </div>
                <div class="col-1-sm d-flex justify-content-end main_menu__bag">
                    <a href="/cart"><i class="fas fa-shopping-bag" style="color:white;"></i></a>
                </div>
            </div>
        </div>
    </div>
</header>
