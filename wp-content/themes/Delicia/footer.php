<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-4 col-12">
            <ul>
                <li>
                    Подпишись на нас
                </li>
                <li class="footer__social">
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-vk"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-5 col-sm-4 col-12">
            <div class="row">
                <div class="col-6 col-sm-12 col-md-6">
                    <ul>
                        <li>
                            <a href="/about">О нас</a>
                        </li>
                        <li>
                            <a href="/shop">Продукция</a>
                        </li>
                        <li>
                            <a href="#">Акции</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 col-sm-12 col-md-6">
                    <ul>
                        <li>
                            <a href="/blog">Блог</a>
                        </li>
                        <li>
                            <a href="/cooperation">Сотрудничество</a>
                        </li>
                        <li>
                            <a href="/#contacts">Контакты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-12">
            <ul>
                <li>
                    Закажи звонок
                </li>
                <li class="op-05">
                    Дарим 15% скидку на первый заказ
                </li>
                <li>
                    <input type="tel" placeholder="Телефон">
                </li>
            </ul>
        </div>
    </div>

</div>
